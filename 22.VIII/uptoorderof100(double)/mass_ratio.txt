type1.txt
Muon electron mass ratio
min: 1                   max: 173.894
Tau electron mass ratio
min: 1.00059             max: 245.996
Tau muon mass ratio
min: 1                   max: 245.996

type2.txt
Muon electron mass ratio
min: 6.64441e+07         max: inf
Tau electron mass ratio
min: 6.64441e+07         max: inf
Tau muon mass ratio
min: 1                   max: 1

type3.txt
Muon electron mass ratio
min: 1.00079             max: 30258
Tau electron mass ratio
min: 2.00079             max: 30259
Tau muon mass ratio
min: 1.00003             max: 1.99921

